FROM phpdockerio/php:8.2-cli

RUN apt-get update; \
    apt-get -y --no-install-recommends install \
        php8.2-mysql \
        php8.2-xdebug; \
    apt-get clean; \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/share/doc/*

COPY xdebug.ini /etc/php/8.2/cli/conf.d/20-xdebug.ini

# Configure xDebug server
ENV PHP_IDE_CONFIG "serverName=docker-xdebug-server"

# Create volume and default dir
RUN mkdir /volume
WORKDIR /volume
VOLUME /volume